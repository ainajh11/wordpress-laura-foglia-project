<?php wp_footer() ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.min.js"></script>
<!-- jQuery for Bootstrap's JavaScript plugins -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
<!-- Bootstrap framework -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.easing.min.js"></script>
<!-- jQuery Easing for smooth scrolling between anchors -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.magnific-popup.js"></script>
<!-- Magnific Popup for lightboxes -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/swiper.min.js"></script>
<!-- Swiper for image and text sliders -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/scripts.js"></script>
</body>

</html>