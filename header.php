<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- SEO Meta Tags -->
    <meta name="description" content="Your description" />
    <meta name="author" content="Your name" />
    <meta property="og:site_name" content="" />
    <meta property="og:site" content="" />
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta name="twitter:card" content="summary_large_image" />
    <title>Laura Foglia</title>
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400;1,600&display=swap" rel="stylesheet" />
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logoheader.png" />
    <?php wp_head() ?>
</head>


<body data-spy="scroll" data-target=".fixed-top">
    <nav class="navbar navbar-expand-lg fixed-top navbar-light">
        <div class="container">
            <a class="navbar-brand logo-image" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logoheader.png" alt="alternative" /></a>

            <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#accueil">Accueil <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#exprentise">Expertise et Conseil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#accompagnement">Accompagnement au changement</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#conferences">Conférences</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#apropos">À propos</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>