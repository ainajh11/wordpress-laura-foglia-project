<?php
get_header();
?>
<!-- Header -->

<header id="accueil">
    <div class="container mt-3">
        <div class="col-lg-12 mb-4 text-center">
            <?php
            $args = array(
                'post_type' => 'les_titres',
                'posts_per_page' => 1
            );
            $$query = new WP_Query($args);
            if ($$query->have_posts()) :
                while ($$query->have_posts()) : $$query->the_post();
                    $acc = get_field('titre_daccueil');
            ?>

                    <h2><?php echo $acc ?></h2>
            <?php
                endwhile;
                wp_reset_postdata();
            endif;
            ?>
        </div>
        <?php
        $args = array(
            'post_type' => 'accueil',
            'posts_per_page' => 1
        );
        $$query = new WP_Query($args);
        if ($$query->have_posts()) :
            while ($$query->have_posts()) : $$query->the_post();
                $acc = get_field('acceuil');
                $xvideo = $acc['video'];
                $description = $acc['description'];
                $logo1 = $acc['logo']['logo_1'];
                $logo2 = $acc['logo']['logo_2'];
                $visible = $acc['visibilite'];

                // var_dump($video);
                // die();
                if ($visible) :
        ?>
                    <div class="row mt-5">
                        <div class="col-lg-6 mb-4">
                            <iframe class="videoH" frameborder="0" allowfullscreen src="<?php echo $xvideo; ?>">
                            </iframe>
                        </div>
                        <div class="col-lg-6">
                            <?php echo $description ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-6 mb-4">
                            <img class="imgH" src="<?php echo $logo1 ?>" alt="... ">
                        </div>
                        <div class="col-lg-3 col-6 mb-4">
                            <img class="imgH" src="<?php echo $logo2 ?>" alt="... ">
                        </div>
                    </div>
        <?php
                endif;
            endwhile;
            wp_reset_postdata();
        endif;
        ?>
    </div>
</header>
<!-- end of header -->
<!-- end of header -->
<!-- *****************   -->
<!-- *****************   -->

<!-- Introduction -->
<div id="exprentise" class="intro mt-3 py-5">
    <div class="container mt-5">
        <div class="row text-center mb-4">
            <?php
            $args = array(
                'post_type' => 'les_titres',
                'posts_per_page' => 1
            );
            $$query = new WP_Query($args);
            if ($$query->have_posts()) :
                while ($$query->have_posts()) : $$query->the_post();
                    $acc = get_field('titre_pour_lexpertise_et_conseil');
            ?>

                    <h2><?php echo $acc ?></h2>
            <?php
                endwhile;
                wp_reset_postdata();
            endif;
            ?>
        </div>
        <div class="row text-center mt-3">
            <?php
            $args = array(
                'post_type' => 'expertise_et_conseil',
                'posts_per_page' => 2
            );
            $$query = new WP_Query($args);
            if ($$query->have_posts()) :
                while ($$query->have_posts()) : $$query->the_post();
                    $exp = get_field('expretise_et_conseil');
                    $titre = $exp['titre'];
                    $description = $exp['description'];
                    $image = $exp['image'];
                    $visible = $exp['visibilite'];
                    if ($visible) :
            ?>
                        <div class="cont col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <img src="<?php echo $image ?>" alt="Avatar" class="imag">
                            <div>
                                <span class="middle">
                                    <?php echo $description ?>
                                </span>
                            </div>
                            <h3 class="text-center p-2">
                                <?php echo $titre ?>
                            </h3>
                        </div>
            <?php
                    endif;
                endwhile;
                wp_reset_postdata();
            endif;
            ?>
        </div>
    </div>
    <!-- end of container -->
</div>
<!-- end of basic-1 -->
<!-- end of introduction -->
<!-- *****************   -->
<!-- *****************   -->
<!-- Features -->
<div id="accompagnement" class="cards-1 pb-0">
    <div class="container">
        <div>
            <?php
            $args = array(
                'post_type' => 'les_titres',
                'posts_per_page' => 1
            );
            $$query = new WP_Query($args);
            if ($$query->have_posts()) :
                while ($$query->have_posts()) : $$query->the_post();
                    $acc = get_field('titre_pour_laccompagnement_au_changement');
            ?>

                    <div class="col-lg-12">
                        <h2 class="col-lg-12">
                            <?php echo $acc['titre'] ?>
                        </h2>
                    </div>
                    <div class="col-lg-12 pt-4 pf">
                        <?php echo $acc['description'] ?>
                    </div>
            <?php
                endwhile;
                wp_reset_postdata();
            endif;
            ?>
            <div class="">
                <p class="pf " style="color: #76b84e; font-weight: bolder;">Laura Foolia a accompagnée :</p>
            </div>
            <div class="col-lg-12  text-center">
                <?php
                $args = array(
                    'post_type' => 'accompagnement',
                    'paged' => 1
                );
                $$query = new WP_Query($args);
                if ($$query->have_posts()) :
                    echo '<div id="show_acc">';
                    while ($$query->have_posts()) : $$query->the_post();
                        $acc = get_field('accompagnement');
                        $logo = $acc['logo'];
                        $description = $acc['description'];
                        $visible = $acc['visibilite'];
                        if ($visible) :
                ?>
                            <div class="imageAccD col-lg-2 col-md-3 col-sm-4 col-xs-1 pr-3 pt-3 mt-2 py-0 px-3">
                                <img class="imageAcc" src="<?php echo $logo ?>" alt="">
                                <div class="content pm pt-3">
                                    <?php echo $description ?>
                                </div>
                            </div>
                <?php
                        endif;
                    endwhile;
                    echo '</div>';
                    wp_reset_postdata();
                endif;

                ?>
                <div style="position: relative;">
                    <button id="load_more_acc" class="btn-solid-lg mr-5 p-2">
                        Read more
                    </button>
                </div>
            </div>
        </div>
        <!-- end of col -->
    </div>
</div>
<!-- end of container -->
</div>
<!-- end of cards-1 -->
<!-- end of features -->

<!-- Début Partie Conférences  -->
<div id="conferences" class="basic-2">
    <div class="container">
        <?php
        $args = array(
            'post_type' => 'sensibilisation',
            'posts_per_page' => 1
        );
        $$query = new WP_Query($args);
        if ($$query->have_posts()) :
            while ($$query->have_posts()) : $$query->the_post();
                $conf = get_field('sensibilite_bas_carbone');
                $titre = $conf['titre'];
                $description = $conf['description'];
                $visible = $conf['visibilite'];
                if ($visible == true) :
        ?>
                    <div class="row">
                        <div class="col-lg-12 partie1">
                            <h2><?php echo $titre ?></h2>
                            <p>
                                <?php echo $description ?>
                            </p>
                        </div>
                    </div>
                    <div class="top-content mb-4">
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner row w-100 mx-auto" role="listbox">
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
                                    <div class="card">
                                        <img class="image-carossel" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/c5.png" alt="" />
                                    </div>
                                </div>
                                <?php
                                $value = get_post_meta($post->ID, 'miu_images', true);
                                $images = unserialize($value);
                                foreach ($images as $image) {
                                    if (!empty($images) && $images != false) {

                                ?>
                                        <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                            <div class="card">
                                                <img src="<?= $image ?>" alt="" class="image-carossel" />
                                            </div>
                                        </div>
                                <?php }
                                } ?>
                            </div>
                            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
            <?php
                endif;
                wp_reset_postdata();
            endwhile;
        endif;
            ?>

            <!-- TY ILAY PARTIE CODE MANAO ANAZY -->
            <div class="container sponso">
                <?php
                $args = array(
                    'post_type' => 'conference',
                    'paged' => 1
                );
                $$query = new WP_Query($args);
                if ($$query->have_posts()) :
                    echo '<div id="show">';
                    while ($$query->have_posts()) : $$query->the_post();
                        $conf = get_field('conference');
                        $desc = $conf['description'];
                        $logo = $conf['logo'];
                        $visible = $conf['visibilite'];
                        // var_dump($visible);
                        // die();
                        if ($visible) :
                ?>

                            <div class="row mb-4 myconference">
                                <div class="col-lg-4 text-center mb-2">
                                    <img src="<?php echo $logo; ?>" alt="" />
                                </div>
                                <div class="pp col-lg-8">
                                    <div class="pa"><?php echo $desc; ?></div>
                                </div>
                            </div>

                <?php
                        endif;
                    endwhile;
                    echo '</div>';
                    wp_reset_postdata();
                endif;
                ?>
                <div class="text-center">
                    <button id="load_more" class="btn btn-primary">Voir plus</button>
                </div>


            </div>
            <!-- FIN ILAY PARTIE CODE  -->

            <div id="temoignage" class="temoignage">
                <div class="row">
                    <div class="col-lg-12 partie1">
                        <?php
                        $args = array(
                            'post_type' => 'les_titres',
                            'posts_per_page' => 1
                        );
                        $$query = new WP_Query($args);
                        if ($$query->have_posts()) :
                            while ($$query->have_posts()) : $$query->the_post();
                                $acc = get_field('titre_temoignage');
                        ?>

                                <h2><?php echo $acc ?></h2>
                        <?php
                            endwhile;
                            wp_reset_postdata();
                        endif;
                        ?>
                    </div>
                </div>
                <div class="slider-2">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center little-title">
                                <p>Lorem ipsum dolor sit amet consectetur.</p>
                            </div>
                            <div class="col-lg-12">
                                <div class="slider-container">
                                    <div class="swiper-container card-slider">
                                        <div class="swiper-wrapper">
                                            <?php
                                            $args = array(
                                                'post_type' => 'temoignage',
                                                'posts_per_page' => -1
                                            );
                                            $$query = new WP_Query($args);
                                            if ($$query->have_posts()) :
                                                while ($$query->have_posts()) : $$query->the_post();
                                                    $tem = get_field('temoignage');
                                                    $profil = $tem['profil'];
                                                    $nom = $tem['nom'];
                                                    $poste = $tem['poste'];
                                                    $temoignage = $tem['temoignage'];
                                                    $visible = $tem['visibilite'];
                                                    // var_dump($visible);
                                                    // die();
                                                    if ($visible) :
                                            ?>
                                                        <div class="swiper-slide">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <p class="testimonial-text">
                                                                        <?php echo $temoignage ?>
                                                                    </p>
                                                                    <div class="details">
                                                                        <img class="testimonial-image" src="<?php echo $profil ?>" alt="alternative" />
                                                                        <div class="text">
                                                                            <div class="testimonial-author">
                                                                                <?php echo $nom ?>
                                                                            </div>
                                                                            <div class="occupation">
                                                                                <?php echo $poste ?>
                                                                            </div>
                                                                        </div>
                                                                        <!-- end of text -->
                                                                    </div>
                                                                    <!-- end of testimonial-details -->
                                                                </div>
                                                            </div>
                                                        </div>
                                            <?php
                                                    endif;
                                                endwhile;
                                                wp_reset_postdata();
                                            endif;
                                            ?>
                                        </div>
                                        <div class="swiper-button-next"></div>
                                        <div class="swiper-button-prev"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    </div>
    </div>
    <!-- À pospos -->
    <div id="apropos" class="basic-2 ">
        <div class="container mt-4 py-2">
            <?php
            $args = array(
                'post_type' => 'apropos',
                'posts_per_page' => 2
            );
            $$query = new WP_Query($args);
            if ($$query->have_posts()) :
                while ($$query->have_posts()) : $$query->the_post();
                    $apropos = get_field('a_propos');
                    $profil = $apropos['profil'];
                    $description = $apropos['description'];
                    $logo1 = $apropos['logo']['image_1'];
                    $logo2 = $apropos['logo']['image_2'];
                    $visible = $apropos['visibilite'];
                    if ($visible) :
            ?>
                        <div class="row">
                            <div class="col-lg-4 text-center mb-4">
                                <img style="width: 300px; height: 280px;" src="<?php echo $profil ?>" alt="">
                            </div>
                            <div class="col-lg-8" style="font-size: 12px;line-height: 20px;">
                                <!-- <p > -->
                                <?php echo $description ?>
                                <!-- </p> -->
                            </div>
                            <div class="col-lg-12">
                                <div class="row text-center">
                                    <div class="col-lg-2 col-6">
                                        <img class="apro-logo" src="<?php echo $logo1 ?>" alt="... ">
                                    </div>
                                    <div class="col-lg-2 col-6">
                                        <img class="apro-logo" src="<?php echo $logo2 ?>" alt="... ">
                                    </div>
                                </div>
                            </div>
                        </div>
            <?php
                    endif;
                endwhile;
                wp_reset_postdata();
            endif;
            ?>

        </div>
        <!-- end of container -->
    </div>


    <?php get_footer() ?>