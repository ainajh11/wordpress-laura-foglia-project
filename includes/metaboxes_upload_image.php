<?php

class Multi_Image_Uploader
{
    public static function register()
    {
        //limit meta box to certain post types
        add_action('add_meta_boxes', [self::class, 'add_meta_box']);
        add_action('save_post', [self::class, 'save']);
        add_action('admin_enqueue_scripts', [self::class, 'laura_foglia_enqueue_metaboxes_scripts']);
    }
    public static function add_meta_box()
    {
        add_meta_box(
            'multi_image_upload_meta_box',
            'Importer les images dans la sensibilé bas carbone',
            [self::class, 'render_meta_box_content'],
            'sensibilisation',
            'advanced',
            'high'
        );

        // var_dump($s);
    }

    public static function save($post_id)
    {
        // Check if our nonce is set.
        if (!isset($_POST['miu_inner_custom_box_nonce']))
            return $post_id;

        $nonce = $_POST['miu_inner_custom_box_nonce'];

        // Verify that the nonce is valid.
        if (!wp_verify_nonce($nonce, 'miu_inner_custom_box'))
            return $post_id;

        // If this is an autosave, our form has not been submitted,
        //     so we don't want to do anything.
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;

        // Check the user's permissions.
        if ('page' == $_POST['post_type']) {

            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } else {

            if (!current_user_can('edit_post', $post_id))
                return $post_id;
        }

        /* OK, its safe for us to save the data now. */

        // Validate user input.
        $posted_images = $_POST['miu_images'];

        $miu_images = array();
        if (!empty($posted_images)) {
            foreach ($posted_images as $image_url) {
                if (!empty($image_url))
                    $miu_images[] = esc_url_raw($image_url);
            }
        }

        // Update the miu_images meta field.
        update_post_meta($post_id, 'miu_images', serialize($miu_images));
    }

    public static function render_meta_box_content($post)
    {

        // Add an nonce field so we can check for it later.
        wp_nonce_field('miu_inner_custom_box', 'miu_inner_custom_box_nonce');

        // Use get_post_meta to retrieve an existing value from the database.
        $value = get_post_meta($post->ID, 'miu_images', true);

        $metabox_content = '<div id="miu_images"></div><input type="button" onClick="addRow()" value="Add Image" class="button" />';
        echo $metabox_content;
?>
        <script>
            function addRow(image_url) {
                // console.log(image_url);
                if (typeof(image_url) === 'undefined') image_url = "";
                itemsCount += 1;
                var emptyRowTemplate = '<div id=row-' + itemsCount + '> <input style=\'float:left;width:50%\' id=img-' + itemsCount + ' type=\'text\' name=\'miu_images[' + itemsCount + ']\' value=\'' + image_url + '\' />' +
                    '&nbsp;<input type=\'button\' href=\'#\' class=\'Image_button button\' id=\'Image_button-' + itemsCount + '\' value=\'Upload\'>' +
                    '&nbsp;<input class="miu-remove button" type=\'button\' value=\'Remove\' id=\'remove-' + itemsCount + '\' />' +
                    '&nbsp;<span>';
                if (image_url) {
                    emptyRowTemplate += '<a target="_blank" href="' + image_url + '"><img style="height: 100px;width: 100px" src="' + image_url + '"></a>';

                }
                emptyRowTemplate += '</span>' +
                    '</div>';
                jQuery('#miu_images').append(emptyRowTemplate);
            }
        </script>
<?php
        $images = unserialize($value);
        $script = "<script> 
            itemsCount= 0;";
        if (!empty($images)) {
            foreach ($images as $image) {
                $script .= "addRow('{$image}');";
            }
        }
        $script .= "</script>";
        echo $script;
    }

    public static function laura_foglia_enqueue_metaboxes_scripts()
    {
        wp_enqueue_script('miu_script', get_template_directory_uri() . '/assets/js/upload_image.js', ['jquery'], null, true);
    }
}
