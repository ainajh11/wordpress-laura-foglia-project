<?php
function import_style()
{
    wp_register_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css', []);
    wp_register_style('tailwind', 'https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css', [], false, true);
    wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', ['popper', 'jquery'], false, true);
    wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', [], false, true);
    // wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://code.jquery.com/jquery-3.2.1.slim.min.js', [], false, true);
    wp_enqueue_style('bootstrap');
    wp_enqueue_script('bootstrap');
    wp_enqueue_style('tailwind');
    wp_enqueue_style('bootstrap', get_template_directory_uri()  . '/assets/css/bootstrap.css');
    wp_enqueue_style('custome-style', get_template_directory_uri()  . '/assets/css/styles.css');
    wp_enqueue_style('swiper', get_template_directory_uri()  . '/assets/css/swiper.css');
    wp_enqueue_style('fontawesome', get_template_directory_uri()  . '/assets/css/fonteawesome-all.css');
    wp_enqueue_style('magnific-popup', get_template_directory_uri()  . '/assets/css/magnific-popup.css');
}
add_action('wp_enqueue_scripts', 'import_style');


function laura_foglia_post_type()
{
    $custom_post_types = ['accueil', 'sensibilisation', 'accompagnement', 'conference', 'temoignage', 'apropos', 'expertise_et_conseil', 'les_titres'];
    $supports = ['title'];
    foreach ($custom_post_types as $custom_post_type) {
        $labels = [
            'name' => _x($custom_post_type, 'plural'),
            'singular_name' => _x($custom_post_type, 'singular'),
            'menu_name' => _x($custom_post_type, 'admin menu'),
            'name_admin_bar' => _x($custom_post_type, 'admin bar'),
            'add_new' => _x('Add New', 'add new'),
            'add_new_item' => __("Add New $custom_post_type"),
            'new_item' => __("New $custom_post_type"),
            'edit_item' => __("Edit $custom_post_type"),
            'view_item' => __("View $custom_post_type"),
            'all_items' => __("All $custom_post_type"),
            'search_items' => __("Search $custom_post_type"),
            'not_found' => __("No $custom_post_type found."),
        ];


        $args = [
            // 'supports' => ,
            'labels' => $labels,
            'public' => true,
            'query_var' => true,
            'rewrite' => ['slug' => rtrim($custom_post_type, 's')],
            'has_archive' => true,
            'hierarchical' => false,
        ];
        register_post_type($custom_post_type, $args);
    }
}
add_action('init', 'laura_foglia_post_type');




function wps_cpt_support()
{
    $var = ['accueil', 'expertise_et_conseil', 'accompagnement', 'conference', 'temoignage', 'sensibilisation', 'apropos', 'les_titres'];
    foreach ($var as $var) {
        remove_post_type_support($var, 'title');
    }
}
add_action('init', 'wps_cpt_support');

add_filter('manage_conference_posts_columns', function ($columns) {
    return [
        'cb' => $columns['cb'],
        'logo' => 'Logo',
        'description' => 'Description',
        'visible' => 'Visibilité',
        // 'title' => $columns['title'],
        'date' => $columns['date']
    ];
});

add_filter('manage_conference_posts_custom_column', function ($column, $post_id) {
    if ($column == 'logo') {
        $a = get_field('conference', $post_id)['logo'];
?>
        <img src="<?php echo $a ?>" alt="" class="logo_conference_admin">
    <?php
    }
    if ($column == 'description') {
        $b = get_field('conference', $post_id)['description'];
    ?>
        <p><?php echo $b ?></p>
    <?php
    }
    if ($column == 'visible') {
        $c = get_field('conference', $post_id)['visibilite'];
        // var_dump($c)
    ?>
        <div class="vis">
            <div class="bullet-<?php if ($c == false) : echo 'no';
                                endif; ?>"></div>
        </div>
    <?php
    }
}, 10, 2);
add_filter('manage_accueil_posts_columns', function ($columns) {
    return [
        'cb' => $columns['cb'],
        'logo' => 'Logo',
        'description' => 'Description',
        'visible' => 'Visibilité',
        // 'title' => $columns['title'],
        'date' => $columns['date']
    ];
});

add_filter('manage_accueil_posts_custom_column', function ($column, $post_id) {
    if ($column == 'logo') {
        $bx = get_field('acceuil', $post_id)['logo']['logo_1'];
        $ax = get_field('acceuil', $post_id)['logo']['logo_2'];

    ?>

        <div class="col-sm-6" style="margin-bottom: 5px;"><img src="<?php echo $bx ?>" alt="" class="logo_conference_admin"></div>
        <div class="col-sm-6"><img src="<?php echo $ax ?>" alt="" class="logo_conference_admin"></div>


    <?php
    }
    if ($column == 'description') {
        $b = get_field('acceuil', $post_id)['description'];
    ?>
        <p><?php echo $b ?></p>
    <?php
    }
    if ($column == 'visible') {
        $c = get_field('acceuil', $post_id)['visibilite'];
        // var_dump($c)
    ?>
        <div class="vis">
            <div class="bullet-<?php if ($c == false) : echo 'no';
                                endif; ?>"></div>
        </div>
    <?php
    }
}, 10, 2);

add_filter('manage_sensibilisation_posts_columns', function ($columns) {
    return [
        'cb' => $columns['cb'],
        'titre' => 'Titre',
        'description' => 'Description',
        'visible' => 'Visibilité',
        // 'title' => $columns['title'],
        'date' => $columns['date']
    ];
});

add_filter('manage_sensibilisation_posts_custom_column', function ($column, $post_id) {
    if ($column == 'titre') {
        $a = get_field('sensibilite_bas_carbone', $post_id)['titre'];
    ?>
        <p><?php echo strtoupper($a) ?></p>
    <?php
    }
    if ($column == 'description') {
        $b = get_field('sensibilite_bas_carbone', $post_id)['description'];
    ?>
        <p><?php echo $b ?></p>
    <?php
    }
    if ($column == 'visible') {
        $c = get_field('sensibilite_bas_carbone', $post_id)['visibilite'];
        // var_dump($c)
    ?>
        <div class="vis">
            <div class="bullet-<?php if ($c == false) : echo 'no';
                                endif; ?>"></div>
        </div>
    <?php
    }
}, 10, 2);




add_filter('manage_accompagnement_posts_columns', function ($columns) {
    return [
        'cb' => $columns['cb'],
        'logo' => 'Logo',
        'description' => 'Description',
        'visible' => 'Visibilité',
        // 'title' => $columns['title'],
        'date' => $columns['date']
    ];
});

add_filter('manage_accompagnement_posts_custom_column', function ($column, $post_id) {
    if ($column == 'logo') {
        $a = get_field('accompagnement', $post_id)['logo'];
    ?>
        <img src="<?php echo $a ?>" alt="" class="logo_conference_admin">
    <?php
    }
    if ($column == 'description') {
        $b = get_field('accompagnement', $post_id)['description'];
    ?> <p style="font-size: 17px;"><?php echo $b ?></p>
    <?php
    }
    if ($column == 'visible') {
        $c = get_field('accompagnement', $post_id)['visibilite'];
        // var_dump($c)
    ?>
        <div class="vis">
            <div class="bullet-<?php if ($c == false) : echo 'no';
                                endif; ?>"></div>
        </div>
    <?php
    }
}, 10, 2);

add_filter('manage_temoignage_posts_columns', function ($columns) {
    return [
        'cb' => $columns['cb'],
        'profil' => 'Profil',
        'nom' => 'Nom',
        'temoignage' => 'Temoignage',
        'visible' => 'Visibilité',
        // 'title' => $columns['title'],
        'date' => $columns['date']
    ];
});

add_filter('manage_temoignage_posts_custom_column', function ($column, $post_id) {
    if ($column == 'profil') {
        $a = get_field('temoignage', $post_id)['profil'];
    ?>
        <img src="<?php echo $a ?>" alt="" class="logo_conference_admin" style="width: 100px;height: 100px; border-radius: 50%;">
    <?php
    }
    if ($column == 'nom') {
        $b = get_field('temoignage', $post_id)['nom'];
    ?>
        <h4><?php echo $b ?></h4>
    <?php
    }
    if ($column == 'temoignage') {
        $b = get_field('temoignage', $post_id)['temoignage'];
    ?>
        <h4><?php echo $b ?></h4>
    <?php
    }
    if ($column == 'visible') {
        $c = get_field('temoignage', $post_id)['visibilite'];
        // var_dump($c)
    ?>
        <div class="vis">
            <div class="bullet-<?php if ($c == false) : echo 'no';
                                endif; ?>"></div>
        </div>
    <?php
    }
}, 10, 2);

add_filter('manage_expertise_et_conseil_posts_columns', function ($columns) {
    return [
        'cb' => $columns['cb'],
        'image' => 'Image',
        'titre' => 'Titre',
        'description' => 'Description',
        'visible' => 'Visibilité',
        // 'title' => $columns['title'],
        'date' => $columns['date']
    ];
});

add_filter('manage_expertise_et_conseil_posts_custom_column', function ($column, $post_id) {
    if ($column == 'image') {
        $a = get_field('expretise_et_conseil', $post_id)['image'];
    ?>
        <img src="<?php echo $a ?>" alt="" class="logo_conference_admin" style="width: 150px;height: 100px;">
    <?php
    }
    if ($column == 'titre') {
        $b = get_field('expretise_et_conseil', $post_id)['titre'];
    ?>
        <h4><?php echo $b ?></h4>
    <?php
    }
    if ($column == 'description') {
        $b = get_field('expretise_et_conseil', $post_id)['description'];
    ?>
        <h4><?php echo $b ?></h4>
    <?php
    }
    if ($column == 'visible') {
        $c = get_field('expretise_et_conseil', $post_id)['visibilite'];
        // var_dump($c)
    ?>
        <div class=" vis">
            <div class="bullet-<?php if ($c == false) : echo 'no';
                                endif; ?>"></div>
        </div>
    <?php
    }
}, 10, 2);


add_filter('manage_apropos_posts_columns', function ($columns) {
    return [
        'cb' => $columns['cb'],
        'profil' => 'Profil',
        'visible' => 'Visibilité',
        'date' => $columns['date']
    ];
});

add_filter('manage_apropos_posts_custom_column', function ($column, $post_id) {
    if ($column == 'profil') {
        $a = get_field('a_propos', $post_id)['profil'];
    ?>
        <img src="<?php echo $a ?>" alt="" class="logo_conference_admin" style="width: 150px;height: 150px;">
    <?php
    }
    if ($column == 'visible') {
        $c = get_field('a_propos', $post_id)['visibilite'];
        // var_dump($c)
    ?>
        <div class=" vis">
            <div class="bullet-<?php if ($c == false) : echo 'no';
                                endif; ?>"></div>
        </div>
    <?php
    }
}, 10, 2);


add_action('admin_enqueue_scripts', function () {
    wp_enqueue_style('admin_laura_foglia', get_template_directory_uri() . '/assets/css/admin.css');
});

require('includes/metaboxes_upload_image.php');
Multi_Image_Uploader::register();


//show more conference function 

add_action('wp_footer', 'my_action_javascript'); // Write our JS below here

function my_action_javascript()
{ ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var post_count = '<?php echo ceil(wp_count_posts('conference')->publish / 5) ?>';

            var ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>';
            var page = 2;

            jQuery('#load_more').click(function() {
                var data = {
                    'action': 'my_action',
                    'page': page
                };
                jQuery.post(ajaxurl, data, function(response) {
                    jQuery('#show').append(response);
                    if (post_count == page) {
                        jQuery('#load_more').hide();
                    }
                    page = page + 1;
                });

            });
        });
    </script>

    <?php
}

add_action('wp_ajax_my_action', 'my_action');
add_action('wp_ajax_nopriv_my_action', 'my_action');

function my_action()
{
    $args = array(
        'post_type' => 'conference',
        'paged' => $_POST['page']
    );
    $$query = new WP_Query($args);
    if ($$query->have_posts()) :
        while ($$query->have_posts()) : $$query->the_post();
            $conf = get_field('conference');
            $desc = $conf['description'];
            $logo = $conf['logo'];
            $visible = $conf['visibilite'];
            if ($visible) :
    ?>

                <div class="row mb-4">
                    <div class="col-lg-4 text-center mb-2">
                        <img src="<?php echo $logo; ?>" alt="" />
                    </div>
                    <div class="pp col-lg-8">
                        <div class="pa"><?php echo $desc; ?></div>
                    </div>
                </div>

    <?php
            endif;
        endwhile;
        wp_reset_postdata();
    endif;
    wp_die(); // this is required to terminate immediately and return a proper response
}

//show more accompagnemt function 

add_action('wp_footer', 'my_action_acc_javascript'); // Write our JS below here

function my_action_acc_javascript()
{ ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var post_count_acc = '<?php echo ceil(wp_count_posts('accompagnement')->publish / 5) ?>';

            var ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>';
            var page = 2;

            jQuery('#load_more_acc').click(function() {
                var data = {
                    'action': 'my_action_acc',
                    'page': page
                };
                jQuery.post(ajaxurl, data, function(response) {
                    jQuery('#show_acc').append(response);
                    if (post_count_acc == page) {
                        jQuery('#load_more_acc').hide();
                    }
                    page = page + 1;
                });

            });
        });
    </script>

    <?php
}

add_action('wp_ajax_my_action_acc', 'my_action_acc');
add_action('wp_ajax_nopriv_my_action_acc', 'my_action_acc');

function my_action_acc()
{
    $args = array(
        'post_type' => 'accompagnement',
        'paged' => $_POST['page']
    );
    $$query = new WP_Query($args);
    if ($$query->have_posts()) :
        while ($$query->have_posts()) : $$query->the_post();
            $acc = get_field('accompagnement');
            $logo = $acc['logo'];
            $description = $acc['description'];
            $visible = $acc['visibilite'];
            if ($visible) :
    ?>

                <div class="imageAccD col-lg-2 col-md-3 col-sm-4 col-xs-1 pr-3 pt-3 mt-2 py-0 px-3">
                    <img class="imageAcc" src="<?php echo $logo ?>" alt="">
                    <div class="content pm pt-3">
                        <?php echo $description ?>
                    </div>
                </div>

<?php
            endif;
        endwhile;
        wp_reset_postdata();
    endif;
    wp_die(); // this is required to terminate immediately and return a proper response
}
